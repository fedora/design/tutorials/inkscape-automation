# Inkscape Automation

Tutorial for how to use Inkscape to automate the production of SVG, PNG, EPS, PS, or PDF graphics using a template file and CSV data input.

## Simple Example

In this first example, we'll just do text replacement. 

You can take a template Inkscape file like this:

![Inkscape file with %VAR_variablename% text in it](https://gitlab.com/fedora/design/tutorials/inkscape-automation/-/raw/main/img/Template_Example_Screenshot.png)

And a CSV file like this:

| ConferenceName  | TalkName                         | PresenterNames     |
|-----------------|----------------------------------|--------------------|
| BestCon         | The Pandas Are Marching          | Beefy D. Miracle   |
| Fedora Nest     | Why Fedora is the Best Linux     | Colúr and Badger   |
| BambooFest 2022 | Bamboo Tastes Better with Fedora | Panda              |
| AwesomeCon      | The Best Talk You Ever Heard     | Dr. Ver E. Awesome |

And get output like this (one per row from the CSV file):
![Image with %VAR_variablename% text filled in with real values from the CSV sheet](https://gitlab.com/fedora/design/tutorials/inkscape-automation/-/raw/main/img/Why_Fedora_is_the_Best_Linux.png)

## Advanced Example

In this second example, we'll replace text, as well as replace various colors, and replace image files in the template.

Here we have a template Inkscape file like this:
![Inkscape file with %VAR_variablename% text in it along with accent colors and a headshot image](https://gitlab.com/fedora/design/tutorials/inkscape-automation/-/raw/main/img/Template_Example_Screenshot2.png)

And a CSV file like this:

| ConferenceName  | TalkName                         | PresenterNames     | TrackNames      | BackgroundColor1 | BackgroundColor2 | AccentColor | Photo      |
|-----------------|----------------------------------|--------------------|-----------------|------------------|------------------|-------------|------------|
| BestCon         | The Pandas Are Marching          | Beefy D. Miracle   | Exercise        | 51a2da           | 294172           | e59728      | beefy.png  |
| Fedora Nest     | Why Fedora is the Best Linux     | Colúr and Badger   | The Best Things | afda51           | 0d76c4           | 79db32      | colur.png  |
| BambooFest 2022 | Bamboo Tastes Better with Fedora | Panda              | Panda Life      | 9551da           | 130dc4           | a07cbc      | panda.png  |
| AwesomeCon      | The Best Talk You Ever Heard     | Dr. Ver E. Awesome | Hyperbole       | da51aa           | e1767c           | db3279      | badger.png |

And JSON-formatted input into the "Non-text values to replace" field in the NextGenerator dialog (these are the default values in the template):

```
{"BackgroundColor1":"51a2da","BackgroundColor2":"294172","AccentColor":"ff0000","Photo":"photo.png"}
```

Like so:
![Screenshot of NextGenerator dialog showing JSON input](https://gitlab.com/fedora/design/tutorials/inkscape-automation/-/raw/main/img/NextGeneratorScreenshot.png)


And get output like this (one per row from the CSV file):
![Image with %VAR_variablename% text filled in with real values from the CSV sheet](https://gitlab.com/fedora/design/team/tutorials/inkscape-automation/-/raw/main/img/Why_Fedora_is_the_Best_Linux2.png)

### Tips to troubleshoot color and image replacement issues

Some hard-won knowledge on how to troubleshoot color and/or image replacement not working:
* Image names are just the filename; keep the images in the same directory as your template and you do not need to use the full file path. (This will make your templates more portable since you can then tar or zip up the directory and share it.)
* Image names and color values and variable names in the spreadsheet *do not* need any " or '. But image names and color values and variable names *DO NEEED THESE* in the JSON.
* Color values are *not* preceded by the *#* character. It won't work if you add it.
* By default Inkscape gives you an 8-"digit" hex value for color codes, the last two correspond to the alpha value of the color (e.g. ff0000ff for bright red with no opacity.) You will need to remove the last two digits so you are using the base 6-"digit" hex code for the color values (that correspond to RGB colors) to remove the opacity/alpha values from the color code. Otherwise, the color replacement won't work.
* Check that you have all variable names in the JSON spelled and written exactly the same as in the CSV header entries except with " in the JSON (e.g. BackgroundColor1 in the CSV is "BackgroundColor1" in the JSON)
* Use the filename for the default image you are replacing in the template. You do not use the ObjectID or any other Inkscape-specific identifier for the image. Also, link the image instead of embedding it.

# Necessary Inkscape Extension

This tutorial uses the Next Generator extension for Inkscape.

Main Next Generator extension repo:
https://gitlab.com/Moini/nextgenerator

Direct Links to download *.inx and *.py for the extension:
https://gitlab.com/Moini/nextgenerator/-/raw/master/next_gen.inx?inline=false
https://gitlab.com/Moini/nextgenerator/-/raw/master/next_gen.py?inline=false

